(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.katsuCurry = {})));
}(this, (function (exports) { 'use strict';

var PLACEHOLDER = "🍛";
var $ = PLACEHOLDER;

'use strict';
var bindInternal3 = function bindInternal3 (func, thisContext) {
  return function (a, b, c) {
    return func.call(thisContext, a, b, c);
  };
};

'use strict';
var some$1 = function fastSome (subject, fn, thisContext) {
  var length = subject.length,
      iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
      i;
  for (i = 0; i < length; i++) {
    if (iterator(subject[i], i, subject)) {
      return true;
    }
  }
  return false;
};

var curry = function (fn) {
  var test = function (x) { return x === PLACEHOLDER; };
  return function curried() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    var countNonPlaceholders = function (toCount) {
      var count = toCount.length;
      while (!test(toCount[count])) {
        count--;
      }
      return count
    };
    var length = (
      some$1(args, test) ?
        countNonPlaceholders(args) :
        args.length
    );
    function saucy() {
      var arguments$1 = arguments;
      var arg2Length = arguments.length;
      var args2 = new Array(arg2Length);
      for (var j = 0; j < arg2Length; ++j) {
        args2[j] = arguments$1[j];
      }
      return curried.apply(this, args.map(
        function (y) { return (
          test(y) && args2[0] ?
            args2.shift() :
            y
        ); }
      ).concat(args2))
    }
    return (
      length >= fn.length ?
        fn.apply(this, args) :
        saucy
    )
  }
};

var innerpipe = function (args) { return function (x) {
  var first = args[0];
  var rest = args.slice(1);
  var current = first(x);
  for (var a = 0; a < rest.length; a++) {
    current = rest[a](current);
  }
  return current
}; };
function pipe() {
  var arguments$1 = arguments;
  var argLength = arguments.length;
  var args = new Array(argLength);
  for (var i = 0; i < argLength; ++i) {
    args[i] = arguments$1[i];
  }
  return innerpipe(args)
}

var prop = curry(function (property, o) { return o && property && o[property]; });

var _assign = Object.assign;
var _keys = Object.keys;
var keys = _keys;
var assign = _assign;


var merge = curry(function (a, b) { return assign({}, a, b); });

var propLength = prop("length");
var objectLength = pipe(keys, propLength);
var length = function (x) { return (typeof x === "object" ? objectLength(x) : propLength(x)); };

var delegatee = curry(function (method, arg, x) { return (x[method](arg)); });

var filter = delegatee("filter");

var flipIncludes = curry(function (list, x) { return list.includes(x); });

var matchingKeys = curry(
  function (list, o) { return filter(
    flipIncludes(list),
    keys(o)
  ); }
);

var matchingKeyCount = curry(
  function (list, o) { return pipe(
    matchingKeys(list),
    length
  )(o); }
);

var expectKArgs = function (expected, args) { return (
  matchingKeyCount(expected, args) >= Object.keys(expected).length
); };
var curryObjectK = curry(
  function (keys, fn) {
    return function λcurryObjectK(args) {
      var includes = function (y) { return keys.includes(y); };
      return (
        Object.keys(args).filter(includes).length === keys.length ?
          fn(args) :
          function (z) { return λcurryObjectK(Object.assign({}, args, z)); }
      )
    }
  }
);

function curryObjectN(arity, fn) {
  return function λcurryObjectN(args) {
    var joined = function (z) { return λcurryObjectN(Object.assign({}, args, z)); };
    return (
      args && Object.keys(args).length >= arity ?
        fn(args) :
        joined
    )
  }
}

function curryObjectKN(ref, fn) {
  var k = ref.k;
  var n = ref.n;
  return function λcurryObjectKN(args) {
    var joined = function (z) { return λcurryObjectKN(Object.assign({}, args, z)); };
    return (
      expectKArgs(k, args) || Object.keys(args).length >= n ?
        fn(args) :
        joined
    )
  }
}

function compose() {
  var arguments$1 = arguments;
  var argLength = arguments.length;
  var args = new Array(argLength);
  for (var i = argLength - 1; i > -1; --i) {
    args[i] = arguments$1[i];
  }
  return innerpipe(args)
}

var curryify = function (test) {
  if (typeof test !== "function") {
    throw new TypeError("Expected to be given a function to test placeholders!")
  }
  return function (fn) {
    if (typeof fn !== "function") {
      throw new TypeError("Expected to be given a function to curry!")
    }
    return function curried() {
      var arguments$1 = arguments;
      var argLength = arguments.length;
      var args = new Array(argLength);
      for (var i = 0; i < argLength; ++i) {
        args[i] = arguments$1[i];
      }
      var countNonPlaceholders = function (toCount) {
        var count = toCount.length;
        while (!test(toCount[count])) {
          count--;
        }
        return count
      };
      var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
      return (
        length >= fn.length ?
        fn.apply(this, args) :
        function saucy() {
          var arguments$1 = arguments;
          var arg2Length = arguments.length;
          var args2 = new Array(arg2Length);
          for (var j = 0; j < arg2Length; ++j) {
            args2[j] = arguments$1[j];
          }
          return curried.apply(this, args.map(
            function (y) { return (
              test(y) && args2[0] ?
              args2.shift() :
              y
            ); }
          ).concat(args2))
        }
      )
    }
  }
};

var version = "0.7.8";

var K = function (x) { return function () { return x; }; };

var I = function (x) { return x; };

var remapParameters = function (indices, arr) {
  var copy = Array.from(arr);
  if (!copy.length) {
    return copy
  }
  return copy.map(
    function (x, index) {
      if (indices.includes(index)) {
        return copy[indices[index]]
      }
      return x
    }
  )
};
var remapArray = curry(remapParameters);
var remapFunction = function (indices, fn) {
  var remapArgs = remapArray(indices);
  var curried = curry(fn);
  return function remappedFn() {
    var args = remapArgs(Array.from(arguments));
    return curried.apply(null, args)
  }
};
var remap = curry(remapFunction);

exports.$ = $;
exports.PLACEHOLDER = PLACEHOLDER;
exports.curry = curry;
exports.curryObjectK = curryObjectK;
exports.curryObjectN = curryObjectN;
exports.curryObjectKN = curryObjectKN;
exports.pipe = pipe;
exports.compose = compose;
exports.curryify = curryify;
exports.version = version;
exports.K = K;
exports.I = I;
exports.remap = remap;
exports.remapArray = remapArray;

Object.defineProperty(exports, '__esModule', { value: true });

})));
