var PLACEHOLDER = "🍛";
var $ = PLACEHOLDER;

'use strict';
var bindInternal3 = function bindInternal3 (func, thisContext) {
  return function (a, b, c) {
    return func.call(thisContext, a, b, c);
  };
};

'use strict';
var some$1 = function fastSome (subject, fn, thisContext) {
  var length = subject.length,
      iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
      i;
  for (i = 0; i < length; i++) {
    if (iterator(subject[i], i, subject)) {
      return true;
    }
  }
  return false;
};

var curry = function (fn) {
  var test = function (x) { return x === PLACEHOLDER; };
  return function curried() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    var countNonPlaceholders = function (toCount) {
      var count = toCount.length;
      while (!test(toCount[count])) {
        count--;
      }
      return count
    };
    var length = (
      some$1(args, test) ?
        countNonPlaceholders(args) :
        args.length
    );
    function saucy() {
      var arguments$1 = arguments;
      var arg2Length = arguments.length;
      var args2 = new Array(arg2Length);
      for (var j = 0; j < arg2Length; ++j) {
        args2[j] = arguments$1[j];
      }
      return curried.apply(this, args.map(
        function (y) { return (
          test(y) && args2[0] ?
            args2.shift() :
            y
        ); }
      ).concat(args2))
    }
    return (
      length >= fn.length ?
        fn.apply(this, args) :
        saucy
    )
  }
};

var delegatee = curry(function (method, arg, x) { return (x[method](arg)); });

var filter = delegatee("filter");

var innerpipe = function (args) { return function (x) {
  var first = args[0];
  var rest = args.slice(1);
  var current = first(x);
  for (var a = 0; a < rest.length; a++) {
    current = rest[a](current);
  }
  return current
}; };
function pipe$1() {
  var arguments$1 = arguments;
  var argLength = arguments.length;
  var args = new Array(argLength);
  for (var i = 0; i < argLength; ++i) {
    args[i] = arguments$1[i];
  }
  return innerpipe(args)
}

var composedToString = function (args, name) {
  if ( args === void 0 ) args = [];
  if ( name === void 0 ) name = "pipe";
  var stringifyFunctions = function (x) { return (
    x && x.toString && typeof x.toString === "function" ?
      x.toString() :
      "fn"
  ); };
  var names = args.map(stringifyFunctions);
  return function () { return (
    (name + "(" + (names.join(", ")) + ")")
  ); }
};

var slice = Array.prototype.slice;
var aintFunction = function (x) { return typeof x !== "function"; };
var prepipe = function (a, name) {
  if ( name === void 0 ) name = "pipe";
  var args = slice.call(a);
  if (args.filter(aintFunction).length > 0) {
    throw new Error(
      (name + " expected all arguments to be functions.")
    )
  }
  return args
};
function pipe() {
  var args = prepipe(arguments);
  var piped = innerpipe(args);
  piped.toString = composedToString(args);
  return piped
}
function compose() {
  var args = prepipe(arguments, "compose").reverse();
  var composed = innerpipe(args);
  composed.toString = composedToString(args, "compose");
  return composed
}

var prop = curry(function (property, o) { return o && property && o[property]; });

var _assign = Object.assign;
var _keys = Object.keys;
var keys = _keys;
var assign = _assign;


var merge$1 = curry(function (a, b) { return assign({}, a, b); });

var propLength = prop("length");
var objectLength = pipe$1(keys, propLength);
var length = function (x) { return (typeof x === "object" ? objectLength(x) : propLength(x)); };

var flipIncludes = curry(function (list, x) { return list.includes(x); });

var matchingKeys = curry(
  function (list, o) { return filter(
    flipIncludes(list),
    keys(o)
  ); }
);

var matchingKeyCount = curry(
  function (list, o) { return pipe$1(
    matchingKeys(list),
    length
  )(o); }
);

var expectKArgs = function (expected, args) { return (
  matchingKeyCount(expected, args) >= Object.keys(expected).length
); };
var curryObjectK$1 = curry(
  function (keys, fn) {
    return function λcurryObjectK(args) {
      var includes = function (y) { return keys.includes(y); };
      return (
        Object.keys(args).filter(includes).length === keys.length ?
          fn(args) :
          function (z) { return λcurryObjectK(Object.assign({}, args, z)); }
      )
    }
  }
);

var join = curry(function (y, x) { return x.join(y); });
var repeat = curry(function (y, x) { return x.repeat(y); });
var split = curry(function (y, x) { return x.split(y); });
var map = curry(function (y, x) { return x.map(y); });
var add = curry(function (y, x) { return x + y; });
var subtract = curry(function (y, x) { return x - y; });
var safeJoin = curry(
  function (joiner, x) { return (
    x.length > 0 ?
      joiner(x) :
      ""
  ); }
);
var wrap = curry(
  function (x, str) { return (
    ("" + (x[0]) + str + (x[1]))
  ); }
);
var parenthesize = wrap("()");
var curlies = wrap("{}");
var commas = join(",");
var toStringJoiner = safeJoin(
  pipe$1(
    commas,
    parenthesize
  )
);
var toObjectStringJoiner = pipe$1(
  safeJoin(pipe$1(
    commas,
    curlies,
    parenthesize
  ))
);
var makeRemainder = curry(function (str, length) { return (
  length > 0 ?
    pipe$1(
      repeat(length),
      split(""),
      commas,
      parenthesize
    )(str) :
    ""
); });
var fillArray = function (x) {
  var list = [];
  while (x > 0) {
    list.push(--x);
  }
  return list.reverse()
};
var question = function (x) { return (
  pipe$1(
    join(":?,"),
    add(x.length > 0 ? ":?" : "")
  )(x)
); };
var without = curry(
  function (x, y) {
    return y.filter(function (z) { return !(x.indexOf(z) > -1); })
  }
);
var keysWhenKeyNumOrRaw = function (x) { return (x && x.k && x.n ? x.k : x); };
var makeObjectRemainder = function (objectKeys, argKeys) {
  if ( objectKeys === void 0 ) objectKeys = [];
  if ( argKeys === void 0 ) argKeys = [];
  return pipe$1(
  keysWhenKeyNumOrRaw,
  without(argKeys),
  question,
  curlies,
  parenthesize
)(objectKeys);
};
var LAMDA_REMAINDER = "?";
var toString = function (fn, args) {
  if ( args === void 0 ) args = [];
  return function () {
  var argString = toStringJoiner(args);
  var remainder = makeRemainder(
    LAMDA_REMAINDER,
    fn.length - args.length
  );
  var name = fn && fn.name || "fn";
  return ("curry(" + name + ")" + argString + remainder)
};
};
var counter = function (x) { return function () { return x++; }; };
var makeNumberObjectRemainder = function (number, keys) {
  var keyLength = keys.length;
  return pipe$1(
    subtract(keyLength),
    fillArray,
    map(add(keyLength)),
    question,
    curlies,
    parenthesize
  )(number)
};
var makeObjectStringSignature = function (name, obj, keys) {
  var argString = toObjectStringJoiner(keys);
  var remainder = makeObjectRemainder(obj, keys);
  return ("" + name + argString + remainder)
};
var makeNumberStringSignature = function (name, number, keys) {
  var remainder = makeNumberObjectRemainder(number, keys);
  return pipe$1(
    map(counter(0)),
    toObjectStringJoiner,
    wrap([name, remainder])
  )(keys)
};
var toObjectString = function (fn, curryCondition, args) {
  if ( curryCondition === void 0 ) curryCondition = [];
  if ( args === void 0 ) args = {};
  return function () {
  var argKeys = Object.keys(args);
  var conditionType = typeof curryCondition;
  var name = "curry(" + (fn && fn.name || "fn") + ")";
  if (conditionType === "number") {
    return makeNumberStringSignature(name, curryCondition, argKeys)
  }
  return makeObjectStringSignature(name, curryCondition, argKeys)
};
};

var merge = curry(function (x, y) { return Object.assign({}, x, y); });
var barfWhen = function (dis) {
  var o = Object.freeze({
    keysAreNotAnArray: function (k) {
      if (!Array.isArray(k)) {
        throw new TypeError((dis + " expected an array of wanted keys."))
      }
      return o
    },
    arityIsNotANumber: function (n) {
      if (typeof n !== "number" || isNaN(n)) {
        throw new TypeError((dis + " expected to be given a number for arity."))
      }
      return o
    },
    noFunctionIsGiven: function (fn) {
      if (typeof fn !== "function") {
        throw new TypeError((dis + " expected to be given a function to curry."))
      }
      return o
    }
  });
  return o
};
function curryObjectKN(ref, fn) {
  var k = ref.k;
  var n = ref.n;
  barfWhen("curryObjectKN")
    .keysAreNotAnArray(k)
    .arityIsNotANumber(n)
    .noFunctionIsGiven(fn);
  function λcurryObjectKN(args) {
    var joined = function (z) { return λcurryObjectKN(merge(args, z)); };
    joined.toString = toObjectString(fn, k, args);
    return (
      expectKArgs(k, args) || length(args) >= n ?
        fn(args) :
        joined
    )
  }
  λcurryObjectKN.toString = toObjectString(fn, k);
  return λcurryObjectKN
}
var curryObjectK = curry(
  function (keys, fn) {
    barfWhen("curryObjectK")
      .keysAreNotAnArray(keys)
      .noFunctionIsGiven(fn);
    function λcurryObjectK(args) {
      var joined = function (z) { return λcurryObjectK(merge(args, z)); };
      joined.toString = toObjectString(fn, keys, args);
      return (
        expectKArgs(keys, args) ?
          fn(args) :
          joined
      )
    }
    λcurryObjectK.toString = toObjectString(fn, keys);
    return λcurryObjectK
  }
);
function curryObjectN(arity, fn) {
  barfWhen("curryObjectN")
    .arityIsNotANumber(arity)
    .noFunctionIsGiven(fn);
  function λcurryObjectN(args) {
    var joined = function (z) { return λcurryObjectN(merge(args, z)); };
    joined.toString = toObjectString(fn, arity, args);
    return (
      Object.keys(args).length >= arity ?
        fn(args) :
        joined
    )
  }
  λcurryObjectN.toString = toObjectString(fn, arity);
  return λcurryObjectN
}

var remapParameters = function (indices, arr) {
  var copy = Array.from(arr);
  if (!copy.length) {
    return copy
  }
  return copy.map(
    function (x, index) {
      if (indices.includes(index)) {
        return copy[indices[index]]
      }
      return x
    }
  )
};
var remapArray$1 = curry(remapParameters);
var remapFunction = function (indices, fn) {
  var remapArgs = remapArray$1(indices);
  var curried = curry(fn);
  return function remappedFn() {
    var args = remapArgs(Array.from(arguments));
    return curried.apply(null, args)
  }
};
var remap$1 = curry(remapFunction);

var curryify = function (test) { return function (fn) {
  if (typeof fn !== "function") {
    throw new TypeError("Expected to be given a function to curry!")
  }
  function curried() {
    var args = Array.from(arguments);
    var countNonPlaceholders = function (toCount) {
      var count = toCount.length;
      while (!test(toCount[count])) {
        count--;
      }
      return count
    };
    var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
    function saucy() {
      var args2 = Array.from(arguments);
      return curried.apply(this, args.map(
        function (y) { return (
          test(y) && args2[0] ?
            args2.shift() :
            y
        ); }
      ).concat(args2))
    }
    saucy.toString = toString(fn, args);
    return (
      length >= fn.length ?
        fn.apply(this, args) :
        saucy
    )
  }
  curried.toString = toString(fn);
  return curried
}; };
var curry$1 = curryify(function (x) { return x === PLACEHOLDER; });
var remapArray = curry$1(remapParameters);
var remap = curry$1(function (indices, fn) {
  var remapArgs = remapArray(indices);
  var curried = curry$1(fn);
  return function remappedFn() {
    var args = remapArgs(Array.from(arguments));
    return curried.apply(null, args)
  }
});

var K = function (x) { return function () { return x; }; };

var I = function (x) { return x; };

var version = "debug";

export { version, pipe, compose, curryObjectK, curryObjectN, curryObjectKN, curry$1 as curry, curryify, remap, remapArray, $, PLACEHOLDER, K, I };
