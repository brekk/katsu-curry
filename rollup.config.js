import resolve from 'rollup-plugin-node-resolve'
import cjs from 'rollup-plugin-commonjs'
import alias from 'rollup-plugin-alias'
import buble from 'rollup-plugin-buble'
import babili from 'rollup-plugin-babili'
import cleanup from 'rollup-plugin-cleanup'
import json from 'rollup-plugin-json'
import progress from 'rollup-plugin-progress'
import prepack from 'rollup-plugin-prepack'
import pkg from './package.json'
import camelCase from 'camel-case'
import path from 'path'

const local = (x) => path.resolve(__dirname, x)
const aliases = {
  "@debug": local(`src/debug`),
  "@placeholder": local(`src/placeholder`),
  "@fp": local(`src/fp`),
  "@combinators": local(`src/combinators`),
  "@params": local(`src/params`),
  "@object": local(`src/object`),
  "@utils": local(`src/utils`)
}
const external = (
  pkg && pkg.dependencies
  ? Object.keys(pkg.dependencies)
  : []
)

const plugins = [
  alias(aliases),
  progress(),
  json(),
  cjs({extensions: [`.js`], include: `node_modules/**`}),
  buble(),
  resolve({jsnext: true, main: true}),
  cleanup({comments: `none`}),
  // babili()
]

export default [
  {
    input: `src/index.js`,
    output: {
      name: camelCase(pkg.name),
      file: pkg.browser,
      format: `umd`
    },
    plugins
  },
  {
    input: `src/index.js`,
    external,
    output: [
      { file: pkg.main, format: `cjs` },
      { file: pkg.module, format: `es` }
    ],
    plugins
  },
  {
    input: `src/debug/index.js`,
    output: {
      name: camelCase(`katsuCurryDebug`),
      file: `debug.umd.js`,
      format: `umd`
    },
    plugins
  },
  {
    input: `src/debug/index.js`,
    external,
    output: [
      { file: `debug.js`, format: `cjs` },
      { file: `debug.es.js`, format: `es` }
    ],
    plugins
  }
]
